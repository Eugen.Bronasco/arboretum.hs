# Arboretum.hs

Haskell package for symbolic manipulation of algebras of graphs, including Butcher series (B-series).

The manual can be found in the **manual** directory.

### Tools used:

autoformat: fourmolu -i <file-name>
    config in fourmolu.yaml

linting: hlint <file-name>

documentation: haddock

testing: doctest, QuickCheck
